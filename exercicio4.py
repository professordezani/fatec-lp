"""
Faça um programa que crie uma lista vazia.
Em seguida, o usuário deverá informar as notas de
trabalhos obtidas (pode variar de 0 até quantos trabalhos
forem informados). Por fim, mostre a média aritmética
das notas obtidas.
"""

notas = [] #list()

while True:
    nota = float(input('Digite a nota: '))
    if nota < 0:
        break
    
    notas.append(nota)

media = sum(notas) / len(notas)
print(round(media, 2))