"""
Dada a frase: "Esta pasta da cor azul é muito boa"
Crie uma aplicação, em python, que remova todas as
stop words (palavras irrelevantes) e retorne a nova
frase após o processamento.
Output: "pasta cor azul muito boa"
"""

from unidecode import unidecode

stop_words = ['a', 'e', 'o', 'que', 'esta', 'este',
'da', 'de']

f = 'Esta pasta da cor azul é muito boa'

# palavras = frase.split()
# nova_frase = []
# for palavra in palavras:
#     if unidecode(palavra.lower()) not in stop_words:
#         nova_frase.append(palavra)

# list comprehension
frase = ' '.join([p for p in f.split() if unidecode(p.lower()) not in stop_words])

print(frase)
