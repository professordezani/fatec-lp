"""
Faça um programa que crie duas listas x e y, com dez números
inteiros cada uma. Em seguida, crie:

a) Uma lista resultante da união de x e y
  (todos os elementos de x e os elementos de y que não estão em x).

b) Uma lista resultante da diferença entre x e y
  (todos os elementos de x que não existam em y).

c) Uma lista resultante da some de x e y
  (soma de cada elemento de x com o elemento de y na mesma posição)
"""

import random

x = random.sample(range(0, 15), 10)
y = random.sample(range(0, 15), 10)

print(x)
print(y)

# item a
uniao = x.copy()
for numero in y:
  if numero not in uniao:
    uniao.append(numero)
print(uniao)

# item b
diferenca = x.copy()
for numero in y:
  if numero in diferenca:
    diferenca.remove(numero)
print(diferenca)

soma = []
for i in range(len(x)):
  soma.append(x[i] + y[i])
print(soma)

# x = [1, 2, 3]
# uniao = x.copy()
# uniao = list(x)
# uniao[0] = 4
# print(x) # 1, 2, 3
