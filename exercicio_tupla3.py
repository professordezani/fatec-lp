"""
Crie um programa que tenha uma tupla totalmente preenchida
com uma contagem por extenso, de zero, até dez.
O programa deverá ler um número pelo teclado (entre 0 e 10)
e mostrá-lo por extenso.
"""

numeros = ('zero', 'um', 'dois', 'três', 'quatro', 'cinco')

numero = int(input('Digite um número entre 0 e 5:'))

print(numeros[numero])