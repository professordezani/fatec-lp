"""
Faça um programa que carregue uma tupla com 5 números inteiros.
Em seguida, crie e mostre uma tupla resultante ordenada
de maneira crescente e crie e mostre uma tupla resultante
ordenada de maneira decrescente.
"""

import random

numeros = tuple(random.sample(range(0, 20), 5))

numeros1 = tuple(sorted(numeros))
numeros2 = tuple(sorted(numeros, reverse=True))
# numeros2 = tuple(numeros1[::-1])