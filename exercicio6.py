"""
Faça um programa em que o usuário deverá digitar
os nomes de dez alunos da sala de aula. Em seguida,
caso o programa encontre nomes repetidos, ele deverá
alterar o nome adicionando o número sequencial.
Por exemplo, se na lista tivermos dois "José",
após o processamento a lista deverá conter
"José 1" e "José 2".
"""

alunos = ['José', 'José', 'Ana', 'Maria', 'José', 'Pedro', 'José', 'Carlos', 'José', 'Ana']

for aluno in alunos:
  if alunos.count(aluno) == 1:
    continue
    
  indice = 1
  i = 0
  for aluno_repetido in alunos:
    if aluno_repetido == aluno:
      alunos[i] = aluno_repetido + ' ' + str(indice)
      indice += 1
    i += 1

print(alunos)
