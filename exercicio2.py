
"""
Faça um programa que carregue uma lista com dez números
inteiros, informados pelo usuário. Em seguida, crie e
mostre uma lista resultante ordenada de maneira crescente
e crie e mostre uma lista resultante ordenada de maneira decrescente.
"""

# numeros = list()

# for i in range(10):
#     numero = int(input())
#     numeros.append(numero)

import random

numeros = random.sample(range(0, 30), 10)
print(numeros)

# numeros.sort()
# print(numeros)

numeros_ordenados = sorted(numeros)
print(numeros_ordenados)

print(numeros_ordenados[::-1])

# numeros_ordenados_dec = sorted(numeros, reverse=True)
# print(numeros_ordenados_dec)

print(numeros)
