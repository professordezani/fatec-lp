"""
Faça um programa que carregue duas tuplas com nomes
de alimentos, com 3 posições cada tupla. Em seguida,
crie uma lista resultante da intercalação dessas
duas tuplas. No final, mostre os itens dessa nova tupla.
"""
alimentos1 = ('arroz', 'feijão', 'frango')
alimentos2 = ('tomate', 'banana', 'ervilha')

alimentos = []

for i in range(len(alimentos1)):
    alimentos.append(alimentos1[i])
    alimentos.append(alimentos2[i])

print(tuple(alimentos))

