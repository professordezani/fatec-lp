"""
Crie uma tupla com os 20 primeiros colocados da Tabela do
Campeonato Brasileiro de Futebol, na ordem de colocação.
Depois, mostre:
- Apenas os 5 primeiros colocados
- Os 4 últimos colocados na tabela
- Uma lista com os times em ordem alfabética
- Em que posição na tabela está o time do São Paulo.
"""

times = ('Flamengo', 'Palmeiras', 'Santos', 'Corinthias', 'Internacional', 'São Paulo', 'Grêmio', 'Bahia', 'Atlético Paranaense', 'Atlético Mineiro', 'Botafogo', 'Goiás', 'Vasco da Gama', 'Ceará', 'Fortaleza', 'Fluminense', 'Cruzeiro', 'CSA', 'Avaí', 'Chapecoense')

# - Apenas os 5 primeiros colocados
print(times[0:5])

# - Os 4 últimos colocados na tabela
print(times[-4:])

# - Uma lista com os times em ordem alfabética
print(sorted(times))

# - Em que posição na tabela está o time do São Paulo.
print(times.index('São Paulo') + 1)

# for index, time in enumerate(times):
#     if time == 'São Paulo':
#         print(index + 1)
