# 1. Data uma lista z = [1, 2, 3, 4, 5], use o list 
# comprehension para gerar uma nova lista com o dobro dos
# valores.
z = [1, 2, 3, 4, 5]

z1 = []
for e in z:
    z1.append(e * 2)

print(z1)

z2 = [e*2 for e in z]
print(z2)

# 2. Dada a lista y = ["joao", "maria", "josé"], use list
# comprehension para colocar os nomes com a primeira letra 
# maiúscula.
y = ["joao", "maria", "josé"]

y1 = [nome.capitalize() for nome in y]
print(y1)

# 3. Dada a lista x = [1, 2, 3, 4, 5], use list comprehension
# para remover os elementos ímpares.
x = [1, 2, 3, 4, 5]

y1 = [i for i in x if i % 2 == 0]
print(y1)