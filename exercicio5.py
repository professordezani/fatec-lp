"""
Implemente o jogo da forca. Um usuário deverá entrar
com uma palavra. Em seguida, outro usuário deverá indicar
as letras dessa palavra. Caso exista, deverá ser mostrada
as letras e as suas posições na palavra. Caso não exista,
o usuário perderá uma chance. No total, o usuário terá
6 chances para acertar.
"""

palavra = input('Digite uma palavra: ')

chances = 6

lista = list(palavra)

lista_view = []
for i in lista:
  lista_view.append('_')

print(' '.join(lista_view))

while True:
  letra = input('Digite uma letra: ')

  if letra not in palavra:
    chances -= 1
    if chances == 0:
      print('Você perdeu.')
      break
    continue

  for i, l in enumerate(palavra):
    if letra == l:
      lista_view[i] = l

  print(' '.join(lista_view))

  if lista_view.count('_') == 0:
    print('Você ganhou')
    break  
