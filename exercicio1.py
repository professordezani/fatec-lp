"""
Faça um programa que carregue duas listas com nomes de animais, 
com 5 posições cada lista. Em seguida, crie uma lista resultante
da intercalação dessas duas listas. No final, mostre os itens
dessa nova lista.
"""

animais1 = ['gato', 'cachorro', 'papagaio', 'tartaruga', 'rato']
animais2 = ['pato', 'leão', 'morcego', 'sapo', 'porco']

resultante = []

for i in range(5):
    resultante.append(animais1[i])
    resultante.append(animais2[i])

print(resultante)