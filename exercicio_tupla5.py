"""
Faça um programa que contenha uma tupla com diversas palavras.
Para cada palavra, mostre suas vogais no seguinte formato:
'A palavra Fatec contém as vogais a e'.
"""
palavras = ('Fatec', 'Banana', 'Tecnologia')

for palavra in palavras:
    vogais = []
    for letra in palavra:
        if letra.lower() in 'aeiou' and letra not in vogais:
            vogais.append(letra)

    print(f'A palavra {palavra} contém as vogais {' '.join(vogais)}.')